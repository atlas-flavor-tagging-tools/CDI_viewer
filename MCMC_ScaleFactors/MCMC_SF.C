

int Fill_Histogram(std::string cdiFile, std::string tagger ,std::string workingpoint ,std::string jetcol, std::string effMap,
	                  TH2D * h_SF, int flavour) {


  	xAOD::TStore store;

  	tool = new BTaggingEfficiencyTool("BTagTest");

  	if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorFileName", cdiFile) ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  	if(StatusCode::SUCCESS != tool->setProperty("TaggerName", tagger)          ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  	if(StatusCode::SUCCESS != tool->setProperty("OperatingPoint",workingpoint) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  	if(StatusCode::SUCCESS != tool->setProperty("JetAuthor",  jetcol)          ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  	if(StatusCode::SUCCESS != tool->setProperty("EfficiencyBCalibrations",      effMap) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  	if(StatusCode::SUCCESS != tool->setProperty("EfficiencyCCalibrations",      effMap) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  	if(StatusCode::SUCCESS != tool->setProperty("EfficiencyTCalibrations",      effMap) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  	if(StatusCode::SUCCESS != tool->setProperty("EfficiencyLightCalibrations",  effMap) ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  	if(StatusCode::SUCCESS != tool->initialize()){std::cout << " failed to initialize tool " << std::endl;}

    xAOD::Jet* myJet = new xAOD::Jet;
    myJet->makePrivateStore();
    myJet->setAttribute("ConeTruthLabelID", flavour);
    myJet->setAttribute("HadronConeExclTruthLabelID",flavour);

    for(int x = 1; x <= h_SF->GetNbinsX(); ++x) {

        for(int y = 1; y <= h_SF->GetNbinsY(); ++y) {

            const double eta = h_SF->GetXaxis()->GetBinCenter(x);
            const double pT = h_SF->GetYaxis()->GetBinCenter(y);

            const int Bin = h_SF->GetBin(x,y);

            xAOD::JetFourMom_t p4Extrapolated(pT*1e3,eta,0.0,1000.0);

            myJet->setJetP4(p4Extrapolated);

            float SF;

            tool->getScaleFactor(*myJet,SF);

            // std::cout << " SF " << SF << std::endl;
            h_SF->SetBinContent(Bin,SF);

        }

    }
    delete myJet;

    delete tool;

    return 0;
}

