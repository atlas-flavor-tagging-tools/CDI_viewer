from array import array
import numpy as np
import ROOT
ROOT.xAOD.Init().ignore()

from ROOT import *


gROOT.ProcessLine('.L MCMC_SF.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)

#CDI_Filename = 'xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2017-07-02_v1.root'
CDI_Filename = 'xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root'

cdifile = TFile('/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/'+CDI_Filename,'read')

tagger = 'MV2c10'
jetCol = 'AntiKt4EMTopoJets'
workingPoint = 'FixedCutBEff_77'

effMap1 = '410500' #pythia8
effMap2 = '410187' #sherpa 2.2.0

flavours =  [[5,'B'], [4,'C'], [0,'Light'], [15,'T']]


c = ROOT.TCanvas('c','c',1400,400)

c.Divide(4)

jetptbins = np.linspace(20,400,200).tolist() #range in GeV
jetetabins = np.linspace(0,2.5,10).tolist()



HistoList = []
LegendList = []

for i,(flavour, flavour_name) in enumerate(flavours):

	c.cd(i+1)


	smoothed_SF_Map1=TH2D("smoothed_SF_Map1","smoothed_SF_Map1",len(jetetabins)-1,array('f',jetetabins),len(jetptbins)-1,array('f',jetptbins))
	smoothed_SF_Map2=TH2D("smoothed_SF_Map2","smoothed_SF_Map2",len(jetetabins)-1,array('f',jetetabins),len(jetptbins)-1,array('f',jetptbins))

	Fill_Histogram(CDI_Filename,tagger,workingPoint,jetCol,effMap1,smoothed_SF_Map1,flavour)
	Fill_Histogram(CDI_Filename,tagger,workingPoint,jetCol,effMap2,smoothed_SF_Map2,flavour)

	directoryName = tagger+'/'+jetCol+'/'+workingPoint+'/'+flavour_name+'/'

	raw_CalibBinned_EffMap_1 = cdifile.Get(directoryName+effMap1+'_CalibrationBinned_Eff').GetValue("result")
	raw_CalibBinned_EffMap_2 = cdifile.Get(directoryName+effMap2+'_CalibrationBinned_Eff').GetValue("result")

	raw_ratioHist = raw_CalibBinned_EffMap_2.ProjectionY().Clone("raw_ratioHist")
	for y in range(1, raw_CalibBinned_EffMap_2.GetNbinsY()+1):

		sum1 = 0
		sum2 = 0

		for x in range(1, raw_CalibBinned_EffMap_2.GetNbinsX()+1):
			sum1+=raw_CalibBinned_EffMap_1.GetBinContent(x,y)
			sum2+=raw_CalibBinned_EffMap_2.GetBinContent(x,y)

		raw_ratioHist.SetBinContent(y,sum1/sum2)

	def project_Hist(h,x_or_y):
		if x_or_y=='x':
			nbins = h.GetNbinsY()
			hnew = h.ProjectionX()
		if x_or_y=='y':
			nbins = h.GetNbinsX()
			hnew = h.ProjectionY()
		hnew.Scale(1.0/nbins)
		return hnew


	pt_SF_map1 = project_Hist(smoothed_SF_Map1,'y')
	pt_SF_map2 = project_Hist(smoothed_SF_Map2,'y')

	pt_SF_map2.Divide(pt_SF_map1)

	pt_SF_map2.SetTitle(tagger+' '+jetCol+' '+workingPoint+' '+flavour_name)
	pt_SF_map2.GetXaxis().SetTitle('p_{T} [GeV]')
	pt_SF_map2.GetYaxis().SetTitle('MC/MC SF')

	pt_SF_map2.GetYaxis().SetRangeUser(0,2)
	pt_SF_map2.SetLineColor(ROOT.kBlack)

	raw_ratioHist.SetLineColor(ROOT.kRed)

	HistoList.append(pt_SF_map2.Clone('pt_SF_map2_'+str(i)))
	HistoList[-1].Draw('HIST')

	HistoList.append(raw_ratioHist.Clone('raw_ratioHist'+str(i)))
	HistoList[-1].Draw('E1 same')

	LegendList.append(ROOT.TLegend(0.5,0.8,0.9,0.9))
	LegendList[-1].AddEntry(HistoList[-1],'Raw')
	LegendList[-1].AddEntry(HistoList[-2],'Smoothed')

	LegendList[-1].Draw()

c.Print('MCMC_SF_'+effMap1+'_'+effMap2+'.pdf')

