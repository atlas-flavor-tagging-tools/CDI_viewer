source the setup.sh script

go into any directory, there will be one python file to extract the desired output (data/MC SF, MC/MC SF, MC efficiency) for a particular working point, tagger, etc, and to make a basic plot.

CDI files are stored in

/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/