from array import array
import numpy as np
import ROOT
ROOT.xAOD.Init().ignore()

from ROOT import *


gROOT.ProcessLine('.L dataMC_SF.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)

def project_Hist(h_old,x_or_y):

	if x_or_y=='x':
		nbins = h_old.GetNbinsY()
		hnew = h_old.ProjectionX()

	if x_or_y=='y':

		nbins = h_old.GetNbinsX()

		hnew = h_old.ProjectionY()

	hnew.Scale(1.0/nbins)

	return hnew


CDI_Filename = 'xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root'


tagger = 'MV2c10'
jetCol = 'AntiKt4EMTopoJets'
workingPoint = 'FixedCutBEff_77'

effMap1 = '410500' #pythia8

flavours =  [[5,'B'], [4,'C'], [0,'Light'], [15,'T']]

c = ROOT.TCanvas('c','c',1400,400)

c.Divide(4)

jetptbins = np.linspace(20,600,100).tolist() #range in GeV
jetetabins = np.linspace(0,2.5,10).tolist()

#plot y axis range
rangeMin,rangeMax = 0, 3.5


HistoList = []

for i,(flavour, flavour_name) in enumerate(flavours):

	c.cd(i+1)


	h_SF=TH2D("h_SF","h_SF",len(jetetabins)-1,array('f',jetetabins),len(jetptbins)-1,array('f',jetptbins))
	Fill_Histogram(CDI_Filename,tagger,workingPoint,jetCol,effMap1,h_SF,flavour, flavour_name)

	h_SF_up = h_SF.Clone('h_SF_up')

	h_SF_down = h_SF.Clone('h_SF_down')


	for x in range(1,h_SF.GetXaxis().GetNbins()+1):
		for y in range(1,h_SF.GetYaxis().GetNbins()+1):

			xval = h_SF.GetXaxis().GetBinCenter(x)
			yval = h_SF.GetYaxis().GetBinCenter(y)

			Bin = h_SF.FindBin(xval,yval)

			h_SF_up.SetBinContent(Bin, h_SF.GetBinContent(Bin) + h_SF.GetBinError(Bin) )
			h_SF_down.SetBinContent(Bin, h_SF.GetBinContent(Bin) - h_SF.GetBinError(Bin) )


	h = project_Hist(h_SF,'y')
	h_up = project_Hist(h_SF_up,'y')
	h_down = project_Hist(h_SF_down,'y')

	h.SetTitle(tagger+' '+jetCol+' '+workingPoint+' '+flavour_name)
	h.SetLineColor(ROOT.kOrange+8)
	h_up.SetLineColor(ROOT.kOrange+8)
	h_up.SetFillColor(ROOT.kOrange+8)
	h_up.SetFillStyle(3636)

	h_down.SetLineColor(ROOT.kOrange+8)
	h_down.SetFillColorAlpha(0,1)

	h.GetXaxis().SetTitle('p_{T} [GeV]')
	h.GetYaxis().SetTitle('Data/MC scale factor')
	h.GetYaxis().SetRangeUser(rangeMin,rangeMax)

 	HistoList.append([h.Clone('h_'+str(i)),h_up.Clone('h_up_'+str(i)),h_down.Clone('h_down_'+str(i)) ])

 	HistoList[-1][0].Draw('HIST')
 	HistoList[-1][1].Draw('HIST same')
 	HistoList[-1][2].Draw('HIST same')
 	ROOT.gPad.RedrawAxis()

c.Print('dataMC_SF_'+effMap1+'.pdf')