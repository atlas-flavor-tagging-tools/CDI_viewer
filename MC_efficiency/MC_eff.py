from array import array
import numpy as np
import ROOT
import ctypes

ROOT.xAOD.Init().ignore()

store = ROOT.xAOD.TStore()

#CDI_Filename = 'xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root'
CDI_Filename = '/Users/jshlomi/Desktop/Athena/CDI_files/2017-21-13TeV-MC16-CDI-July10_2019.root'

tagger = 'DL1'
jetCol = 'AntiKt4EMTopoJets'
workingPoint = 'FixedCutBEff_60'

effMap = '410470' 

flavour = 'B'
flav_int = 5 

cdi_file = ROOT.TFile(CDI_Filename,'read')
raw_hist = cdi_file.Get(tagger+'/'+jetCol+'/'+workingPoint+'/'+flavour+'/'+effMap+'_Eff').GetValue('result')


btagtool = ROOT.BTaggingEfficiencyTool('tool')
btagtool.setProperty('ScaleFactorFileName',CDI_Filename)
btagtool.setProperty("TaggerName", tagger)   
btagtool.setProperty("OperatingPoint",     workingPoint)
btagtool.setProperty("JetAuthor",           jetCol)
btagtool.setProperty('Efficiency'+flavour+'Calibrations',effMap)


btagtool.initialize()


jet_vars = ROOT.Analysis.CalibrationDataVariables()
jet_vars.jetPt  = 250000
jet_vars.jetEta = 0.34
eff = ctypes.c_float() 

c = ROOT.TCanvas('c','c',800,800)


jetptbins = np.linspace(0,500,100).tolist() 
jetetabins = np.linspace(0,2.5,20).tolist()


h_Eff=ROOT.TH2D("h_Eff","h_Eff",len(jetptbins)-1,array('f',jetptbins),len(jetetabins)-1,array('f',jetetabins))


for bin_pt in range(1,len(jetptbins)+1):
	jet_vars.jetPt  = h_Eff.GetXaxis().GetBinCenter(bin_pt)*1000.0
	for bin_eta in range(1,len(jetetabins)+1):
		jet_vars.jetEta =  h_Eff.GetYaxis().GetBinCenter(bin_eta)

		btagtool.getMCEfficiency(flav_int, jet_vars,eff)
		h_Eff.SetBinContent(bin_pt, bin_eta, eff.value )

		raw_hist_bin = raw_hist.FindBin(jet_vars.jetPt/1000.0,jet_vars.jetEta)

		effuncert = raw_hist.GetBinError(raw_hist_bin)
		h_Eff.SetBinError(bin_pt, bin_eta, effuncert )


h_Eff.Draw('COLZ')

c.Print('test.pdf')