import ROOT
import os
import collections

ROOT.xAOD.Init().ignore()

from ROOT import *

def createpage(cdifilename, pagename,tagger,jetcol,wp,wpinfo):

	htmlfile = open(pagename,'w')

	htmlfile.write('<head>\n')
	htmlfile.write('<style>\n')
	htmlfile.write('.nobr { white-space: nowrap }\n')
	htmlfile.write('.accordion {background-color: #eee;color: #444;cursor: pointer; padding: 18px; width: 100%; border: none; text-align: left;outline: none; font-size: 15px;transition: 0.4s;}')
	htmlfile.write('.active, .accordion:hover {background-color: #ccc; }')
	htmlfile.write('.panel { padding: 0 18px; display: none;  background-color: white;}\n')
	htmlfile.write('</style>\n')
	htmlfile.write('</head>\n')

	htmlfile.write('<h1>'+tagger+' '+jetcol+' '+wp+'</h1>\n')

	writtenmaps = False
	for flav in wpinfo:
		maplist = wpinfo[flav]['maplist']
		mchad_ref = wpinfo[flav]['mc_had_ref']

		if mchad_ref:
			if writtenmaps:
				continue
			for mc_ref in mchad_ref:

				htmlfile.write('<h4>'+str(mc_ref)+' '+str(mchad_ref.GetValue(mc_ref))+'</h4>\n')
			writtenmaps = True
		else:
			htmlfile.write('<h3> no efficiency maps found </h3>\n')
			return
	
	flavours = ['B','C','Light','T']

	pt_ranges ={}
	pt_ranges['dataMCSF']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
	pt_ranges['calibEff']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
	pt_ranges['mcmcSF']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
	pt_ranges['Eff']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}

	def write_Flav_Menu(obj_name, flavname,ptrange):
		htmlfile.write(' <a href="'+obj_name+'_'+flavname+'_'+str(ptrange)+'.pdf"> pt < '+str(ptrange)+' [GeV] '+'</a> &nbsp;')

	def write_Object_Menu(obj_name,shortname):
		htmlfile.write('<LI>')
		htmlfile.write('<button class="accordion">'+obj_name+'</button>\n')
		htmlfile.write('<div class="panel">\n')
		htmlfile.write('<p>\n')
		htmlfile.write('<UL>\n')
		for flav in flavours:
			htmlfile.write('<LI class=nobr> '+flav+' ')
			for ptval in pt_ranges[shortname][flav]:
				write_Flav_Menu(shortname,flav,ptval)
			htmlfile.write('</LI>  \n')
		htmlfile.write('</UL>\n')
		htmlfile.write('</p>\n')
		htmlfile.write('</div>\n')


	htmlfile.write('<UL>\n')

	write_Object_Menu('DataMC scale factors','dataMCSF')
	write_Object_Menu('Calibration Binned Efficiency Maps','calibEff')
	write_Object_Menu('MC MC scale factors','mcmcSF')
	write_Object_Menu('Finely Binned Efficiency Maps','Eff')

	htmlfile.write('</UL>\n')

	htmlfile.write("""<script>var acc = document.getElementsByClassName("accordion");var i;
	for (i = 0; i < acc.length; i++) {
	    acc[i].addEventListener("click", function() {
	        this.classList.toggle("active");
	        var panel = this.nextElementSibling;
	        if (panel.style.display === "block") {
	            panel.style.display = "none";
	        } else {
	            panel.style.display = "block";
	        }
	    });
	}
	</script>""")





