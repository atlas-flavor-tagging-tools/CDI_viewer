from array import array
import numpy as np
import ROOT
import sys
ROOT.xAOD.Init().ignore()

from ROOT import *

# ROOT.gROOT.ProcessLine('.L mc_eff/mc_eff.C')

# gROOT.SetBatch(1)
# gStyle.SetOptStat(0)
# ROOT.gStyle.SetPalette(ROOT.kBird)

# ROOT.gROOT.ProcessLine('.L AtlasStyle.C')
# ROOT.SetAtlasStyle()
def getOrMkdir (dir, name):
    dirName = dir.Get(name)
    if dirName:
        return dirName
    return dir.mkdir(name)

def create_Eff_plot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,map_type,ptrange_dict):
	flipHist = False
	texlist = []

	def draw_tex(flav):
		texlist.append( ROOT.TLatex(0.2,0.88,"ATLAS") )
		texlist[-1].SetNDC();
		texlist[-1].SetTextFont(72);
		texlist[-1].SetLineWidth(2);
		texlist[-1].Draw('same');
		texlist.append( ROOT.TLatex(0.36,0.88," Simulation Internal") )
		texlist[-1].SetNDC();
		texlist[-1].SetTextFont(42);
		texlist[-1].SetLineWidth(2);
		texlist[-1].Draw("same");
		texlist.append( ROOT.TLatex(0.2,0.82,"#sqrt{s} = 13 TeV") )

		texlist[-1].SetNDC();
		texlist[-1].SetTextFont(42);
		texlist[-1].SetLineWidth(2);
		texlist[-1].Draw('same');

		texlist.append( ROOT.TLatex(0.2,0.78,'#scale[0.8]{'+tagger+' '+wp+' '+flav+'}') )

		texlist[-1].SetNDC();
		texlist[-1].SetTextFont(42);
		texlist[-1].SetLineWidth(2);
		texlist[-1].Draw('same');

		texlist.append( ROOT.TLatex(0.2,0.75,'#scale[0.4]{'+jetcol+'}') )

		texlist[-1].SetNDC();
		texlist[-1].SetTextFont(42);
		texlist[-1].SetLineWidth(2);
		texlist[-1].Draw('same');


	

	cdifile = ROOT.TFile(CDIname,'read')

	tagdir = cdifile.Get(tagger)
	jetcoldir = tagdir.Get(jetcol)
	wpdir = jetcoldir.Get(wp)

	flav_number = {'B':5,'C':4,'T':15,'Light':0}

	for flav in ['B','C','T','Light']:

		ptranges = ptrange_dict[map_type][flav]
		for ptrange in ptranges:

			mchad_ref = wpdir.Get(flav+'/MChadronisation_ref')
			defaultSF = wpdir.Get(flav+'/default_SF')

			maplist = []
			calibbinned_maplist = []
			sfobjectlist = []
			
			flavdir = wpdir.Get(flav)
			for objectlist in flavdir.GetListOfKeys():
				if 'Eff' in objectlist.GetName():
					maplist.append( objectlist.GetName() )
				if 'SF' in objectlist.GetName():
					sfobjectlist.append( flavdir.Get(objectlist.GetName()) )


			topdir = tagger+'/'+jetcol+'/'+wp+'/'+flav+'/'
			effmap_list = []

			effcolor = {0:ROOT.kRed,1:ROOT.kBlue,2:ROOT.kGreen+3,3:ROOT.kOrange,4:ROOT.kGray,5:ROOT.kMagenta,6:ROOT.kCyan,7:ROOT.kYellow,8:ROOT.kAzure,9:ROOT.kSpring-1}
			mapnames = {'410501':'default Pythia8','410250':'Sherpa2.2','410558':'Herwig7','410000':'Pythia6',
						 '426131':'Sherpa_dijet','364443':'Herwig7_dijet','361020':'Pythia8EvtGen_dijet','410470':'Pythia8','410464':'Py8_aMCNLO'}

			for effname in maplist:
				if map_type=='calibEff' and 'CalibrationBinned' not in effname:
					continue
				elif  map_type=='Eff' and 'CalibrationBinned' in effname:
					continue
				if 'purified' in effname or 'default' in effname:
					continue
				
				dsid = effname[0:6]
				if dsid in mapnames:
					mapname =  mapnames[effname[0:6]]
				else:
					mapname = dsid

				effmap_list.append([topdir+effname,mapname, effcolor[len(effmap_list)],dsid ])

			CDIfile = ROOT.TFile(CDIname,'r')

			effmaps = {}

			#this script assumes all maps have the same eta binning
			nEtaBins = -1
			etaInfo = {}

			def flip_hist(hist):

				xaxis = hist.GetXaxis().GetXbins().GetArray()
				yaxis = hist.GetYaxis().GetXbins().GetArray()

				newhist = ROOT.TH2D(hist.GetName(),hist.GetName(),hist.GetNbinsY(),yaxis,hist.GetNbinsX(),xaxis)

				for bin_x in range(1,hist.GetNbinsX()+1):
					for bin_y in range(1,hist.GetNbinsY()+1):
						bincont = hist.GetBinContent(bin_x,bin_y)
						binerr = hist.GetBinError(bin_x,bin_y)

						newhist.SetBinContent(bin_y,bin_x,bincont)
						newhist.SetBinError(bin_y,bin_x,binerr)

				return newhist

			for ef in effmap_list:
				effmaps[ef[1]] = {}
				if flipHist:
					effmaps[ef[1]]['hist'] =  flip_hist(CDIfile.Get(ef[0]).GetValue('result'))
				else:
					effmaps[ef[1]]['hist'] =  CDIfile.Get(ef[0]).GetValue('result')
				effmaps[ef[1]]['nEtaBins'] = effmaps[ef[1]]['hist'].GetYaxis().GetNbins()
				effmaps[ef[1]]['ptbinning'] =  effmaps[ef[1]]['hist'].GetXaxis().GetXbins()
				nPtBins = effmaps[ef[1]]['hist'].GetXaxis().GetNbins()
				effmaps[ef[1]]['maxpt'] = effmaps[ef[1]]['ptbinning'].GetArray()[nPtBins]
				effmaps[ef[1]]['minpt'] = effmaps[ef[1]]['ptbinning'].GetArray()[0]
				effmaps[ef[1]]['dsid'] = ef[3]

				# if effmaps[ef[1]]['maxpt'] > ptrange:
				# 	ptrange = int(effmaps[ef[1]]['maxpt']+200)
				ptlist = []
				for pt_i in range(nPtBins+1):
					ptlist.append( effmaps[ef[1]]['ptbinning'].GetArray()[pt_i] )
				#print ef[1],' : ',ptlist

				nEtaBins = effmaps[ef[1]]['nEtaBins'] 
				
				if len(etaInfo)==0:
					etabins = effmaps[ef[1]]['hist'].GetYaxis().GetXbins().GetArray()
					for i in range(nEtaBins):
						etaInfo[i] = [ etabins[i], etabins[i+1] ]


			def draw_eta_tex(i):
				
				texlist.append( ROOT.TLatex(0.2,0.71,'{0:.2f}'.format(etaInfo[i][0])+' < #eta < '+'{0:.2f}'.format(etaInfo[i][1])) )

				texlist[-1].SetNDC();
				texlist[-1].SetTextFont(42);
				texlist[-1].SetLineWidth(2);
				texlist[-1].Draw('same');


			canvas_name = 'c_'+tagger+'_'+wp+'_'+jetcol+'_'+flav+'_'+map_type
			c = ROOT.TCanvas(canvas_name,canvas_name,550*nEtaBins,1600)
			c.Divide(nEtaBins,2)

			hlist = {}
			leglist = {} 


			maxEffValue = 0
			minEffValue = 1

			for map_option in ['Raw','Smooth']:

				for eta_i in range(1,nEtaBins+1):
							
					for i,ef in enumerate(effmap_list):
						
						hname = 'h_'+map_option+'_'+str(eta_i)+'_'+str(i)
						
						effmap = effmaps[ef[1]]
						
						smoothbins = ptrange
						eta_central_val = 0.5*(etaInfo[eta_i-1][0]+etaInfo[eta_i-1][1])

						hlist[hname] = ROOT.TH1F(hname,hname,smoothbins,0,ptrange)

						for pt_i in range(1,smoothbins+1):
							ptval = hlist[hname].GetBinCenter(pt_i)
							
							if map_option == 'Smooth':
								if(ptval >= effmap['maxpt']):
									ptval = effmap['maxpt']-1
								if(ptval<= effmap['minpt'] ):
									ptval = effmap['minpt']+1

								effvalue = effmap['hist'].Interpolate(ptval,eta_central_val)
								binerr = effmap['hist'].GetBinError(effmap['hist'].FindBin(ptval,eta_central_val))


							if map_option == 'Raw':
								if(ptval > effmap['maxpt'] or ptval< effmap['minpt']):
									effvalue = 0
									binerr = 0
								else:
									effvalue = effmap['hist'].GetBinContent(effmap['hist'].FindBin(ptval,eta_central_val))
									binerr = effmap['hist'].GetBinError(effmap['hist'].FindBin(ptval,eta_central_val))

							if effvalue+binerr > maxEffValue:
								maxEffValue = effvalue+binerr
							if effvalue-binerr < minEffValue:
								minEffValue = effvalue-binerr

							hlist[hname].SetBinContent(pt_i,effvalue)
							hlist[hname].SetBinError(pt_i,binerr)


			for map_option in ['Raw','Smooth']:

				for eta_i in range(1,nEtaBins+1):
					
					if map_option == 'Raw':
						c.cd(eta_i)
					else:
						c.cd(eta_i+nEtaBins)

					leglist[map_option+'_'+str(eta_i)] = ROOT.TLegend(0.2,0.7-0.02*len(effmap_list),0.65,0.7)

					
					for i,ef in enumerate(effmap_list):
						
						hname = 'h_'+map_option+'_'+str(eta_i)+'_'+str(i)

						hlist[hname].SetMarkerSize(0)
						hlist[hname].SetLineWidth(1)
						hlist[hname].GetXaxis().SetTitle('pT [GeV]')
						
						hlist[hname].GetYaxis().SetTitle('Efficiency')
						hlist[hname].GetYaxis().SetTitleOffset(1.5)

						Max_efficiency = 2.1*maxEffValue
						Min_efficiency = max(0,minEffValue-0.5*(maxEffValue-minEffValue))
						

						hlist[hname].GetYaxis().SetRangeUser(Min_efficiency,Max_efficiency)
						hlist[hname].GetXaxis().SetRangeUser(0,ptrange)
						hlist[hname].GetXaxis().SetNdivisions(5)
						hlist[hname+'_clone'] = hlist[hname].Clone(hname+'_clone')
						hlist[hname+'_clone'].SetLineColor(ef[2])
						hlist[hname].SetFillColorAlpha(ef[2],0.1)
					
						hlist[hname].Draw('E2 same')
						hlist[hname+'_clone'].Draw('HIST same')
					
						hlist[hname+'_forlegend'] = hlist[hname+'_clone'].Clone(hname+'_forlegend')
						hlist[hname+'_forlegend'].SetFillColorAlpha(ef[2],0.3)

						leglist[map_option+'_'+str(eta_i)].AddEntry(hlist[hname+'_forlegend'],ef[3]+' '+ef[1]+' ('+map_option+')','F')

					leglist[map_option+'_'+str(eta_i)].Draw('same')
					draw_tex(flav)
					draw_eta_tex(eta_i-1)



			if map_type=='calibEff':
				c.Print(pagename[0:-10]+'/calibEff_'+flav+'_'+str(ptrange)+'.pdf')
			else:
				c.Print(pagename[0:-10]+'/Eff_'+flav+'_'+str(ptrange)+'.pdf')
		
			del c






