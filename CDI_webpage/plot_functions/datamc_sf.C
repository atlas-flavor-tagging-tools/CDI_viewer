

int Fill_SF_Histogram(std::string cdiFile, std::string tagger ,std::string workingpoint ,std::string jetcol, std::string sfObject,
    TH2D * h_Sf, int flavour, std::string FlavName) {

  int retval = 0;

  xAOD::TStore store;

  BTaggingEfficiencyTool* tool= new BTaggingEfficiencyTool("BTagTest");

  if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorFileName", cdiFile) ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  if(StatusCode::SUCCESS != tool->setProperty("TaggerName", tagger)          ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  if(StatusCode::SUCCESS != tool->setProperty("OperatingPoint",     workingpoint)        ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  if(StatusCode::SUCCESS != tool->setProperty("JetAuthor",           jetcol) ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  //choose between 'SFEigen' and 'Envelope'
  if(StatusCode::SUCCESS != tool->setProperty("SystematicsStrategy","Envelope") ){ std::cout << "error initializing tool " << std::endl; return -1;  }

  if(FlavName=="B"){
      if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorBCalibration",      sfObject) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  }
  if(FlavName=="C"){
  if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorCCalibration",      sfObject) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  }
  if(FlavName=="Light"){
  if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorLightCalibration",      sfObject) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  }
  if(FlavName=="T"){
  if(StatusCode::SUCCESS != tool->setProperty("ScaleFactorTCalibration",      sfObject) ){ std::cout << "error initializing tool " << std::endl; return -1;  }
  }
  tool->setProperty("OutputLevel", MSG::WARNING).ignore();
  StatusCode code = tool->initialize();


  if (code != StatusCode::SUCCESS ) {
    std::cout << "Initialization of tool " << tool->name() << " failed! " << std::endl;
    return -1;
  }
  else {
    std::cout << "Initialization of tool " << tool->name() << " finished." << std::endl;
  }


    CP::SystematicSet up_var;

    CP::SystematicSet systs = tool->affectingSystematics();
    for( CP::SystematicSet::const_iterator iter = systs.begin(); iter!=systs.end(); ++iter) {

      CP::SystematicVariation var = *iter;
      TString SystName = var.name();
      std::cout << SystName << std::endl;



      if("FT_EFF_"+FlavName+"_systematics__1up"==SystName && FlavName!="T"){
        up_var.insert(var);
      }
      if(FlavName=="T"){
          if("FT_EFF_C_systematics__1up"==SystName ){
            up_var.insert(var);
          }

      }

    }

    CP::SystematicSet extrap_syst;
    CP::SystematicVariation extrap = CP::SystematicVariation("FT_EFF_extrapolation__1up");
    extrap_syst.insert(extrap);

    CP::SystematicSet tau_extrap_syst;
    CP::SystematicVariation tau_extrap = CP::SystematicVariation("FT_EFF_extrapolation_from_charm__1up");
    tau_extrap_syst.insert(tau_extrap);


    xAOD::Jet* myJet = new xAOD::Jet;
    myJet->makePrivateStore();
    myJet->setAttribute("ConeTruthLabelID", flavour);
    myJet->setAttribute("HadronConeExclTruthLabelID",flavour);

    for(int x = 1; x <= h_Sf->GetNbinsX(); ++x) {

        for(int y = 1; y <= h_Sf->GetNbinsY(); ++y) {

            const double eta = h_Sf->GetYaxis()->GetBinCenter(y);
            const double pT = h_Sf->GetXaxis()->GetBinCenter(x);

            const int Bin = h_Sf->GetBin(x,y);

            xAOD::JetFourMom_t p4Extrapolated(pT*1e3,eta,0.0,1000.0);

            myJet->setJetP4(p4Extrapolated);

            float sf;
            float sf_up_var;
            float sf_extrap_var;


            tool->getScaleFactor(*myJet,sf);

            h_Sf->SetBinContent(Bin,sf);


            tool->applySystematicVariation(up_var);

            tool->getScaleFactor(*myJet,sf_up_var);

            CP::SystematicSet defaultSet;
            tool->applySystematicVariation(defaultSet);

            tool->applySystematicVariation(extrap_syst);

            tool->getScaleFactor(*myJet,sf_extrap_var);

            tool->applySystematicVariation(defaultSet);

            float tau_var=sf;

            if(FlavName=="T"){

              tool->applySystematicVariation(tau_extrap_syst);

              tool->getScaleFactor(*myJet,tau_var);

              tool->applySystematicVariation(defaultSet);

            }

            float err = sqrt( (sf-sf_up_var)*(sf-sf_up_var)+(sf-sf_extrap_var)*(sf-sf_extrap_var)+(sf-tau_var)*(sf-tau_var) );

            //float err = fabs(sf-sf_up_var);

            h_Sf->SetBinError(Bin,err);

        }

    }
    delete myJet;

    delete tool;

    return retval;
}