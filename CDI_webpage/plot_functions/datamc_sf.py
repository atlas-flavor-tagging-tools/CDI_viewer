from array import array
#import numpy as np
import ROOT
import sys
ROOT.xAOD.Init().ignore()

from ROOT import *

ROOT.gROOT.ProcessLine('.L plot_functions/datamc_sf.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(ROOT.kBird)

def getOrMkdir (dir, name):
    dirName = dir.Get(name)
    if dirName:
        return dirName
    return dir.mkdir(name)

def getErrorHists(hist):

	h_SF_up = hist.Clone(hist.GetName()+'_up')

	h_SF_down = hist.Clone(hist.GetName()+'_down')

	for x in range(1,hist.GetXaxis().GetNbins()+1):
		for y in range(1,hist.GetYaxis().GetNbins()+1):

			xval = hist.GetXaxis().GetBinCenter(x)
			yval = hist.GetYaxis().GetBinCenter(y)

			Bin = hist.FindBin(xval,yval)

			h_SF_up.SetBinContent(Bin, hist.GetBinContent(Bin) + hist.GetBinError(Bin) )
			h_SF_down.SetBinContent(Bin, hist.GetBinContent(Bin) - hist.GetBinError(Bin) )
	return [h_SF_up, h_SF_down]

def project_Hist(h,x_or_y):
	if x_or_y=='x':
		nbins = h.GetNbinsY()
		hnew = h.ProjectionX()
	if x_or_y=='y':
		nbins = h.GetNbinsX()
		hnew = h.ProjectionY()
	hnew.Scale(1.0/nbins)
	return hnew


def createSFplot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,ptrange_dict,outputROOTfilename):

	print jetcol,' ',tagger,' ',wp


	#try to get the number of eta bins in default SF from raw CDI
	rawcdi = ROOT.TFile(rawCDIname,'read')




	cdifile = ROOT.TFile(CDIname,'read')

	tagdir = cdifile.Get(tagger)
	jetcoldir = tagdir.Get(jetcol)
	wpdir = jetcoldir.Get(wp)

	flav_number = {'B':5,'C':4,'T':15,'Light':0}

	for flav in ['B','C','T','Light']:

		ptranges = ptrange_dict['dataMCSF'][flav]
		for ptrange in ptranges:

			mchad_ref = wpdir.Get(flav+'/MChadronisation_ref')
			defaultSF = wpdir.Get(flav+'/default_SF')

			default_raw_SFhist = rawcdi.Get(tagger+'/'+jetcol+'/'+wp+'/'+flav+'/default_SF')

			nEtaBins = 1
			etabinning = [0,2.5]
			rawCDI_ok = True

			if default_raw_SFhist:
				nEtaBins = default_raw_SFhist.GetValue('result').GetXaxis().GetNbins()
				etaarray = default_raw_SFhist.GetValue('result').GetXaxis().GetXbins().GetArray()
				etabinning = []
				for eta_i in range(nEtaBins+1):
					etabinning.append( etaarray[eta_i] )

			else:
				rawCDI_ok = False




			maplist = []
			calibbinned_maplist = []
			sfobjectlist = []
			
			flavdir = wpdir.Get(flav)
			for objectlist in flavdir.GetListOfKeys():
				if 'Eff' in objectlist.GetName():
					maplist.append( objectlist.GetName() )
				if 'SF' in objectlist.GetName():
					#print objectlist.GetName()
					sfobjectlist.append( flavdir.Get(objectlist.GetName()) )


			n_ScaleFactors = len(sfobjectlist)
			
			if n_ScaleFactors < 2:
				return

			if n_ScaleFactors > 2: #if its 2 sf, its actually only 1 sf, the default, but there is always a copy of the default named "default"
				c = ROOT.TCanvas('c','c',800,800*n_ScaleFactors)
				c.Divide(nEtaBins,n_ScaleFactors+1)
			else:
				c = ROOT.TCanvas('c','c',800,800)
				#c.Divide(nEtaBins,1)			
			
			sfcolor = {}
			sfcolor['smooth'] = [ROOT.kBlack,ROOT.kBlue,ROOT.kRed+1,ROOT.kOrange]
			sfcolor['raw'] = [ROOT.kBlack]

			hlist = {}
			maxSF = 0
			minSF = 100000

			for i, sfobject in enumerate( sfobjectlist ):
				
				sfname = sfobject.GetName()
				hlist[sfname] =  ROOT.TH2D(sfname,sfname,ptrange,0,ptrange,nEtaBins,0,2.5)
				hlist[sfname+'_raw'] = ROOT.TH2D(sfname+'_raw',sfname+'_raw',ptrange,0,ptrange,nEtaBins,0,2.5)
				
				Fill_SF_Histogram(CDIname, tagger , wp ,jetcol, sfobject.GetName()[0:-3],hlist[sfname] , flav_number[flav], flav)
				if rawCDI_ok:
					Fill_SF_Histogram(rawCDIname, tagger , wp ,jetcol, sfobject.GetName()[0:-3],hlist[sfname+'_raw'] , flav_number[flav], flav)
				
				h_SF_up, h_SF_down = getErrorHists(hlist[sfname])
				smooth_SF = project_Hist(hlist[sfname],'x')
				h_up = project_Hist(h_SF_up,'x')
				h_down = project_Hist(h_SF_down,'x')	
				
				hlist[sfname+'_smooth_SF'] = smooth_SF.Clone()
				hlist[sfname+'_smooth_SF'].GetXaxis().SetNdivisions(5)
				
				hlist[sfname+'_smooth_SF_up'] = h_up.Clone()
				hlist[sfname+'_smooth_SF_down'] = h_down.Clone()

				if rawCDI_ok:
					raw_up, raw_down = getErrorHists(hlist[sfname+'_raw'])
					raw_SF = project_Hist(hlist[sfname+'_raw'],'x')
					raw_pt_up = project_Hist(raw_up,'x')
					raw_pt_down = project_Hist(raw_down,'x')
					hlist[sfname+'_raw_SF'] = raw_SF.Clone()
					hlist[sfname+'_raw_SF'].GetXaxis().SetNdivisions(5)
					
					hlist[sfname+'_raw_SF_up'] = raw_pt_up.Clone()
					hlist[sfname+'_raw_SF_down'] = raw_pt_down.Clone()
			
				if maxSF < h_up.GetMaximum():
					maxSF = h_up.GetMaximum()
				if minSF > h_down.GetMinimum():
					minSF = h_down.GetMinimum()

			texlist = []
			

			def draw_tex():
				texlist.append( ROOT.TLatex(0.2,0.88,"ATLAS") )
				texlist[-1].SetNDC();
				texlist[-1].SetTextFont(72);
				texlist[-1].SetLineWidth(2);
				texlist[-1].Draw('same');
				texlist.append( ROOT.TLatex(0.36,0.88," Simulation Internal") )
				texlist[-1].SetNDC();
				texlist[-1].SetTextFont(42);
				texlist[-1].SetLineWidth(2);
				texlist[-1].Draw("same");
				texlist.append( ROOT.TLatex(0.2,0.82,"#sqrt{s} = 13 TeV") )

				texlist[-1].SetNDC();
				texlist[-1].SetTextFont(42);
				texlist[-1].SetLineWidth(2);
				texlist[-1].Draw('same');
		

			#print 'pagename ', pagename[0:-10]

			leglist = []
			leglist.append( ROOT.TLegend(0.5,0.75,0.8,0.85))
			drew_tex = False
			
			for i, sfobject in enumerate( sfobjectlist ):
				
				leglist.append( ROOT.TLegend(0.5,0.75,0.8,0.85))
				c.cd(i+2)
				
				sfname = sfobject.GetName()
					
				sfrange = maxSF - minSF

				for sf_type in ['smooth','raw']:
					if sf_type=='raw' and not rawCDI_ok:
						continue
					hlist[sfname+'_'+sf_type+'_SF'].GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)

					hlist[sfname+'_'+sf_type+'_SF'].SetMarkerSize(0)
					hlist[sfname+'_'+sf_type+'_SF'].GetYaxis().SetTitle('data/MC SF')
					hlist[sfname+'_'+sf_type+'_SF'].GetXaxis().SetTitle('pT [GeV]')

					hlist[sfname+'_'+sf_type+'_SF'].SetLineColor(sfcolor[sf_type][i % len(sfcolor[sf_type])])
					hlist[sfname+'_'+sf_type+'_SF'].SetFillColorAlpha(sfcolor[sf_type][i % len(sfcolor[sf_type])],0.1)
				
					hlist[sfname+'_'+sf_type+'_SF'].Draw('E2 same')
					
					hlist[sfname+'_'+sf_type+'_SF_central'] = hlist[sfname+'_'+sf_type+'_SF'].Clone()
					hlist[sfname+'_'+sf_type+'_SF_central'].SetFillColor(0)
					hlist[sfname+'_'+sf_type+'_SF_central'].Draw('HIST SAME')

					leglist[-1].AddEntry(hlist[sfname+'_'+sf_type+'_SF'],sfname+'('+sf_type+')','F')
				
				leglist[-1].Draw('same')
				

				draw_tex()

				
				if 'default' in sfname:
					continue
				
				c.cd(1)
				
				
				hlist[sfname+'_smooth_SF'].Draw('E2 same')
				hlist[sfname+'_smooth_SF_central'].Draw('HIST SAME')

				if n_ScaleFactors==2:
					if rawCDI_ok:
						hlist[sfname+'_raw_SF'].Draw('E2 same')
						hlist[sfname+'_raw_SF_central'].Draw('HIST SAME')
						leglist[0].AddEntry(hlist[sfname+'_raw_SF'],sfname+'(raw)','F')
						
					leglist[0].AddEntry(hlist[sfname+'_smooth_SF'],sfname+'(smooth)','F')
				else:
					leglist[0].AddEntry(hlist[sfname+'_smooth_SF'],sfname,'F')
				
				if not drew_tex:
					draw_tex()
					drew_tex = True
			c.cd(1)
			leglist[0].Draw('same')

			outfile = ROOT.TFile(outputROOTfilename,'update')
			
			outf_tagdir = getOrMkdir(outfile,tagger)
			outf_jetCdir  = getOrMkdir(outf_tagdir,jetcol)
			outf_wpdir = getOrMkdir(outf_jetCdir,wp)
			outf_wpdir.WriteTObject(c,'dataMCSF_'+flav+'_'+str(ptrange))
			outfile.Close()

			c.Print(pagename[0:-10]+'/dataMCSF_'+flav+'_'+str(ptrange)+'.C')
			c.Print(pagename[0:-10]+'/dataMCSF_'+flav+'_'+str(ptrange)+'.pdf')




