import ROOT
import os
import json
import sys

ROOT.xAOD.Init().ignore()

from ROOT import *
from plot_functions import mc_eff
from plot_functions import mcmc_sf
from plot_functions import datamc_sf

ROOT.gROOT.ProcessLine('.L AtlasStyle.C')


ROOT.SetAtlasStyle()

f =  open('pages_to_generate.txt', 'r')

lines = f.readlines()

configs = []
for line in lines:
	configs.append( json.loads(line) )

firstmap_index = int(sys.argv[1])
lastmap_index = int(sys.argv[2])

print 'n jobs ',len(configs)

pt_ranges ={}
pt_ranges['dataMCSF']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
pt_ranges['calibEff']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
pt_ranges['mcmcSF']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}
pt_ranges['Eff']= {'B':[600,1500],'C':[400,1500],'Light':[600,1500],'T':[400,1500]}

outputROOTfileName = 'plots_'+str(firstmap_index)+'_'+str(lastmap_index)+'.root'


for config_i in range(firstmap_index,lastmap_index):

	pagename = str(configs[config_i]['pagename'])
	CDIname = str(configs[config_i]['CDI']     )
	rawCDIname = str(configs[config_i]['rawCDI']     )
	tagger = str(configs[config_i]['tagger']	)
	jetcol = str(configs[config_i]['jetcol'] 	)
	wp = str(configs[config_i]['wp'])

	datamc_sf.createSFplot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,pt_ranges,outputROOTfileName)

	mc_eff.create_Eff_plot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,'Eff',pt_ranges)
	mc_eff.create_Eff_plot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,'calibEff',pt_ranges)

	mcmc_sf.createMCMCSF(CDIname, rawCDIname, pagename,tagger,jetcol,wp,'410501',pt_ranges)

