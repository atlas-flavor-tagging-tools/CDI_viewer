import ROOT
import os
import collections
import json

from helper_functions import *

CDI_title = ''

CDIname =  '2017-21-13TeV-MC16-CDI.root'
CDItitle = '2017-21-13TeV-MC16-CDI-2018-10-19_v1.root'
#CDIpath = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/xAODBTaggingEfficiency/13TeV/'
CDIpath = '/Users/jshlomi/Desktop/Athena/CDI_files/Oct_2018_CDI/'

rawCDI_path=CDIpath+'2017-21-13TeV-MC16-CDI.root' #'2017-21-13TeV-MC16-CDI-2018-06-29_BEFORE_v1.root'

CDIfile = ROOT.TFile(CDIpath+CDIname,'r')

topdir = CDIfile.GetListOfKeys()

taggers = collections.OrderedDict()

for key in topdir:
	if key.GetName() !='VersionInfo':
		taggers[key.GetName()]=collections.OrderedDict()


htmlout = open('index.html','w')
htmlout.write('<head>\n<title>'+CDItitle+'</title>\n')
htmlout.write('<style>\n')
htmlout.write('.nobr { white-space: nowrap }\n')
htmlout.write('.accordion {background-color: #eee;color: #444;cursor: pointer; padding: 18px; width: 100%; border: none; text-align: left;outline: none; font-size: 15px;transition: 0.4s;}')
htmlout.write('.active, .accordion:hover {background-color: #ccc; }')
htmlout.write('.panel { padding: 0 18px; display: none;  background-color: white;}\n')
htmlout.write('</style>\n')
htmlout.write('</head>\n')
htmlout.write('<h1>'+CDItitle+'</h1>\n')

if not os.path.exists('pages'):
	os.mkdir('pages')

def MakeDir(tagger,jetcol,wp):

	for toplevel in ['pages']:

		if os.path.exists(toplevel+'/'+tagger+'/'+jetcol+'/'+wp):
			continue

		if not os.path.exists(toplevel+'/'+tagger):
			os.mkdir(toplevel+'/'+tagger)
		if not os.path.exists(toplevel+'/'+tagger+'/'+jetcol):
			os.mkdir(toplevel+'/'+tagger+'/'+jetcol)
		os.mkdir(toplevel+'/'+tagger+'/'+jetcol+'/'+wp)


for tagger in taggers:

	tagdir = CDIfile.Get(tagger)

	jetCols = tagdir.GetListOfKeys()

	for jetcol in [key.GetName() for key in jetCols]:

		taggers[tagger][jetcol]=collections.OrderedDict()

		jetcoldir = tagdir.Get(jetcol)

		for workingPoint in [key.GetName() for key in jetcoldir.GetListOfKeys()]:
			# if workingPoint=='Continuous':
			# 	continue

			taggers[tagger][jetcol][workingPoint] = collections.OrderedDict()
			wpdir = jetcoldir.Get(workingPoint)

			for flav in [key.GetName() for key in wpdir.GetListOfKeys()]:
				if flav not in ['B','C','T','Light']:
					continue
				taggers[tagger][jetcol][workingPoint][flav] = collections.OrderedDict()

				mchad_ref = wpdir.Get(flav+'/MChadronisation_ref')
				defaultSF = wpdir.Get(flav+'/default_SF')

				taggers[tagger][jetcol][workingPoint][flav]['mc_had_ref'] = mchad_ref
				taggers[tagger][jetcol][workingPoint][flav]['defaultSF'] = defaultSF

				maplist = []
				sfobjectlist = []
				flavdir = wpdir.Get(flav)
				for objectlist in flavdir.GetListOfKeys():
					if 'Eff' in objectlist.GetName():
						maplist.append( objectlist.GetName() )
					if 'SF' in objectlist.GetName():
						sfobjectlist.append( flavdir.Get(objectlist.GetName()) )

				taggers[tagger][jetcol][workingPoint][flav]['maplist'] = maplist
				taggers[tagger][jetcol][workingPoint][flav]['sflist'] = sfobjectlist

htmlout.write('<UL>\n')

pageindex = -1

for tagger in taggers:

	# if tagger not in ['DL1','MV2c10']:
	# 	continue
	htmlout.write('<LI>'+tagger+'\n')
	htmlout.write('<button class="accordion">Show</button>\n')
	htmlout.write('<div class="panel">\n')
	htmlout.write('<p>\n')

	htmlout.write('<UL>\n')
	for jetcol in taggers[tagger]:

		# if 'EMTopo' not in jetcol:
		# 	continue

		htmlout.write('<LI> '+jetcol+'\n')
		htmlout.write('<button class="accordion">Show</button>\n')
		htmlout.write('<div class="panel">\n')
		htmlout.write('<p>\n')
		htmlout.write('<UL>\n')
		for wp in taggers[tagger][jetcol]:
			
			# if wp!='CTag_Tight':
			# 	continue


			if len(taggers[tagger][jetcol][wp]) > 0:

				MakeDir(tagger,jetcol,wp)
				pagename = 'pages/'+tagger+'/'+jetcol+'/'+wp+'/plots.html'
				pageindex+=1
				print pageindex, ' ', pagename

				htmlout.write('<LI class=nobr> <h6> <a href="'+pagename+'">'+wp+'</a> </h6></LI>  \n')

				createpage(CDIpath+CDIname, pagename,tagger,jetcol,wp,taggers[tagger][jetcol][wp])

				#create_SF_page(rawCDI_path, CDIpath+CDIname, SF_pagename,tagger,jetcol,wp,taggers[tagger][jetcol][wp])

				page_cfg = {}
				page_cfg['pagename'] = pagename
				page_cfg['CDI'] = CDIpath+CDIname
				page_cfg['rawCDI'] = rawCDI_path
				page_cfg['tagger'] = tagger
				page_cfg['jetcol'] = jetcol
				page_cfg['wp'] = wp
				
				with open('pages_to_generate.txt', 'a') as file:
					file.write(json.dumps(page_cfg)+'\n')


		htmlout.write('</UL>\n')
		htmlout.write('</p>\n')
		htmlout.write('</div>\n')

	htmlout.write('</UL>\n')
	htmlout.write('</p>\n')
	htmlout.write('</div>\n')
htmlout.write('</UL>\n')


htmlout.write("""<script>var acc = document.getElementsByClassName("accordion");var i;
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
</script>""")

