Descoped from Jonathan's CDI viewer. For now I've only update the mc-to-data scale factor plotting script.
It produces plots ready for pulbic plots approval for a given CDI.

Recipe:

Log onto an lxplus machine 

source setup.sh

You have to prepare an input text file such as pages\_to\_generate.txt, it should be self  explanatory.
Basically in this file you define what CDI files to use, what Jet collections, tagger and wp to use. All the entries will be looped.


To run the script, you need to execute the following command:

python create\_plots.py "Type" "Iteration" "Run"

Type corresponds to whether you want to make data calibration plots ("Normal") or extrapolation plots ("Extra") as the plot formats are different.
Iteration tells the code whether the plots will be marked as "Internal" or "Preliminary", anything you set.
Run tells the code what CME and Lumi to use.

If these two arguments are not parsed or parsed incorrectly the script will fail

Also if you already have the same output directories the code fails too.

For instance, to make the final plots:

python create\_plots.py Extra Preliminary Run2  

plot\_functions/cosmetics.py contains the central cosmetic setups
plot\_functions/datamc\_sf.py is the actual plotting script
plot\_functions/datamc\_sf.C is a C macro to retrieve info from the CDI

Now we also have scripts to make plots for pseudo-continuous calibrations

Just do: 

python create\_plots\_PC\_ptBin.py Normal Internal Run2
python create\_plots\_PC\_tagBin.py Normal Internal Run2

The corresponding macros are:

plot\_functions/datamc\_sf\_PC\_tagBin.py
plot\_functions/datamc\_sf\_PC\_tagBin.C 

and:

plot\_functions/datamc\_sf\_PC\_ptBin.py
plot\_functions/datamc\_sf\_PC\_ptBin.C 
 



 
