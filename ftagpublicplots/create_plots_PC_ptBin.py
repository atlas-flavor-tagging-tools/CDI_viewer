import ROOT
import os
import json
import sys

ROOT.xAOD.Init().ignore()

from ROOT import *
from plot_functions import datamc_sf_PC_ptBin, cosmetics

ROOT.gROOT.ProcessLine('.L AtlasStyle.C')

ROOT.SetAtlasStyle()

plotType = sys.argv[1]
plotIteration = sys.argv[2]
run = sys.argv[3]

f =  open('pages_to_generate.txt', 'r')

lines = f.readlines()

configs = []
for line in lines:
  configs.append( json.loads(line) )

for config_i in range(0,len(configs)):
 
  pagename = str(configs[config_i]['pagename'])
  CDIname = str(configs[config_i]['CDI']) 
  rawCDIname = str(configs[config_i]['rawCDI'])
  tagger = str(configs[config_i]['tagger'])
  jetcol = str(configs[config_i]['jetcol'])
  wp = str(configs[config_i]['wp'])
 
  directory = pagename.split("/")[0]  + "/" + tagger + "/" + jetcol + "/" + wp 
  os.makedirs(directory)

  pt_ranges = {}

  outputROOTfileName = pagename.split("/")[2] + "_" + pagename.split("/")[1] + "_" + pagename.split("/")[3] + "_" + plotType  + ".root" 

  datamc_sf_PC_ptBin.createSFplot(rawCDIname, pagename,tagger,jetcol,wp,pt_ranges,outputROOTfileName, plotType, plotIteration, run)


