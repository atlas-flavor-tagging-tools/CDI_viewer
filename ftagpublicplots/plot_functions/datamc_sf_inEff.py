from array import array
import ROOT
import sys
ROOT.xAOD.Init().ignore()

from ROOT import *
from plot_functions.cosmetics import *

ROOT.gROOT.ProcessLine('.L plot_functions/datamc_sf_inEff.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(ROOT.kBird)


def getOrMkdir (dir, name):
    dirName = dir.Get(name)
    if dirName:
        return dirName
    return dir.mkdir(name)

def getErrorHists(hist):
  h_SF_up = hist.Clone(hist.GetName()+'_up')
  h_SF_down = hist.Clone(hist.GetName()+'_down')

  for x in range(1,hist.GetXaxis().GetNbins()+1):
    for y in range(1,hist.GetYaxis().GetNbins()+1):
      xval = hist.GetXaxis().GetBinCenter(x)
      yval = hist.GetYaxis().GetBinCenter(y)
      Bin = hist.FindBin(xval,yval)
      h_SF_up.SetBinContent(Bin, hist.GetBinContent(Bin) + hist.GetBinError(Bin) )
      h_SF_down.SetBinContent(Bin, hist.GetBinContent(Bin) - hist.GetBinError(Bin) )
  return [h_SF_up, h_SF_down]

def project_Hist(h,x_or_y):
  if x_or_y=='x':
    nbins = h.GetNbinsY()
    hnew = h.ProjectionX()
  if x_or_y=='y':
    nbins = h.GetNbinsX()
    hnew = h.ProjectionY()
  hnew.Scale(1.0/nbins)
  return hnew

def createInEffSFplot(CDIname, rawCDIname, pagename,tagger,jetcol,wp,ptrange_dict,outputROOTfilename, plotType, iteration = "Internal", run = "Run2"):
   print (jetcol,' ',tagger,' ',wp)
   #try to get the number of eta bins in default SF from raw CDI
   rawcdi = ROOT.TFile(rawCDIname,'read')
   cdifile = ROOT.TFile(CDIname,'read')
   tagdir = cdifile.Get(tagger)
   jetcoldir = tagdir.Get(jetcol) 
   wpdir = jetcoldir.Get(wp)
   flav_number = {'B':5,'C':4,'T':15,'Light':0}
   
   for flav in ['B','C','T','Light']:
     ptranges = ptrange_dict['dataMCSF'][flav]
     for ptrange in ptranges:
       mchad_ref = wpdir.Get(flav+'/MChadronisation_ref')
       defaultSF = wpdir.Get(flav+'/default_SF')
       default_raw_SFhist = rawcdi.Get(tagger+'/'+jetcol+'/'+wp+'/'+flav+'/default_SF')

       nEtaBins = 1
       etabinning = [0,2.5]
       rawCDI_ok = True
       if True:
         if default_raw_SFhist:
           nEtaBins = default_raw_SFhist.GetValue('result').GetXaxis().GetNbins()
           etaarray = default_raw_SFhist.GetValue('result').GetXaxis().GetXbins().GetArray()
           etabinning = []
           for eta_i in range(nEtaBins+1):
             etabinning.append( etaarray[eta_i] )
         else:
           rawCDI_ok = False

         maplist = []
         calibbinned_maplist = []
         sfobjectlist = []
                        
         flavdir = wpdir.Get(flav)
         for objectlist in flavdir.GetListOfKeys():
           if 'Eff' in objectlist.GetName():
             maplist.append( objectlist.GetName() )
           if 'SF' in objectlist.GetName():
             sfobjectlist.append( flavdir.Get(objectlist.GetName()) )

         n_ScaleFactors = len(sfobjectlist)
         if n_ScaleFactors < 2:
           return
         sfcolor = {}
         sfcolor['smooth'] = [ROOT.kBlack,ROOT.kBlue,ROOT.kRed+1,ROOT.kOrange]
         sfcolor['raw'] = [ROOT.kBlack]
         hlist = {}
         maxSF = 0
         minSF = 100000
         c = ROOT.TCanvas('c','c',800,800)
         if plotType == "Extra":
           ROOT.gPad.SetLogx()
         for i, sfobject in enumerate( sfobjectlist ):
           sfname = sfobject.GetName()
           if not labelDic.has_key(sfname):
             if "default" not in sfname:
               continue 
           starting = startDic[jetcol][flav] 
           hlist[sfname] =  ROOT.TH2D(sfname,sfname,ptrange,starting,ptrange,nEtaBins,0,2.5)
           hlist[sfname+'_raw'] = ROOT.TH2D(sfname+'_raw',sfname+'_raw',ptrange,starting,ptrange,nEtaBins,0,2.5)
           Fill_inEffSF_Histogram(CDIname, tagger , wp ,jetcol, sfobject.GetName()[0:-3],hlist[sfname] , flav_number[flav], flav)
           if rawCDI_ok:
             Fill_inEffSF_Histogram(rawCDIname, tagger , wp ,jetcol, sfobject.GetName()[0:-3],hlist[sfname+'_raw'] , flav_number[flav], flav)
                                
           h_SF_up, h_SF_down = getErrorHists(hlist[sfname])
           smooth_SF = project_Hist(hlist[sfname],'x')
           h_up = project_Hist(h_SF_up,'x')
           h_down = project_Hist(h_SF_down,'x')        
                                
           hlist[sfname+'_smooth_SF'] = smooth_SF.Clone()
           hlist[sfname+'_smooth_SF'].GetXaxis().SetNdivisions(5)
                                
           hlist[sfname+'_smooth_SF_up'] = h_up.Clone()
           hlist[sfname+'_smooth_SF_down'] = h_down.Clone()

           if rawCDI_ok:
             raw_up, raw_down = getErrorHists(hlist[sfname+'_raw'])
             raw_SF = project_Hist(hlist[sfname+'_raw'],'x')
             raw_pt_up = project_Hist(raw_up,'x')
             raw_pt_down = project_Hist(raw_down,'x')
             hlist[sfname+'_raw_SF'] = raw_SF.Clone()
             hlist[sfname+'_raw_SF'].GetXaxis().SetNdivisions(5)
                                        
             hlist[sfname+'_raw_SF_up'] = raw_pt_up.Clone()
             hlist[sfname+'_raw_SF_down'] = raw_pt_down.Clone()
                        
           if maxSF < h_up.GetMaximum():
             maxSF = h_up.GetMaximum()
           if minSF > h_down.GetMinimum():
             minSF = h_down.GetMinimum()

         texlist = []
        
         def draw_tex():
           texlist.append( ROOT.TLatex(0.2,0.88,"ATLAS") )
           texlist[-1].SetNDC();
           texlist[-1].SetTextFont(72);
           texlist[-1].SetLineWidth(2);
           texlist[-1].SetTextSize(0.04);
           texlist[-1].Draw('same');
           texlist.append( ROOT.TLatex(0.33,0.88," " + iteration) )
           texlist[-1].SetNDC();
           texlist[-1].SetTextFont(42);
           texlist[-1].SetLineWidth(2);
           texlist[-1].SetTextSize(0.03);
           texlist[-1].Draw("same");
           if run == "Run3":
             texlist.append( ROOT.TLatex(0.65,0.88,"#sqrt{s} = 13.6 TeV, 29 fb^{-1}") )
           else:
             texlist.append( ROOT.TLatex(0.65,0.88,"#sqrt{s} = 13 TeV, 139 fb^{-1}") )
 
           texlist[-1].SetNDC();
           texlist[-1].SetTextFont(42);
           texlist[-1].SetLineWidth(1);
           texlist[-1].SetTextSize(0.03);
           texlist[-1].Draw('same');

         leglist = []
         leglist.append( ROOT.TLegend(0.19,0.65,0.79,0.75))
         taggerString = tagger
         if tagger == "MV2c10":
           taggerString = "MV2"
         leglist[-1].SetHeader(colDic[jetcol])
         leglist[-1].SetTextFont(42)
         leglist[-1].SetTextSize(0.03)
         leglist[-1].SetLineColor(1)
         leglist[-1].SetLineStyle(1)
         leglist[-1].SetLineWidth(1)
         leglist[-1].SetFillColor(0)
         leglist[-1].SetBorderSize(0)
         leglist[-1].SetFillStyle(0)
         drew_tex = False
         for i, sfobject in enumerate( sfobjectlist ):
                                
           c.cd()  
           
           sfname = sfobject.GetName()
           if not labelDic.has_key(sfname):
             if "default" not in sfname:
               continue 
           sfrange = maxSF - minSF
           h = c.DrawFrame(20,minSF-0.3*sfrange,ptrange,maxSF+0.8*sfrange)
           h.GetXaxis().SetTitle("p_{T} [GeV]")
           h.GetXaxis().SetTitleOffset(1.5)
           h.GetYaxis().SetTitle(flavDic[flav] + "-jet " + flavTypeDic[flav] + " SFs" )
           h.GetYaxis().SetTitleOffset(1.6)
           c.Modified()
           for sf_type in ['smooth','raw']:
             if sf_type=='raw' and not rawCDI_ok:
               continue
             if sf_type=='smooth':
               numberOfPoints = hlist[sfname+'_'+sf_type+'_SF'].GetNbinsX() 
               x_up = array('d')               
               y_up = array('d')                
               exl_up = array('d')                 
               exh_up = array('d')        
               eyl_up = array('d')                 
               eyh_up = array('d')                 
               x_down = array('d')               
               y_down = array('d')                
               exl_down = array('d')                 
               exh_down = array('d')        
               eyl_down = array('d')                 
               eyh_down = array('d')                 
               x_c = array('d')               
               y_c = array('d')                
               exl_c = array('d')                 
               exh_c = array('d')        
               eyl_c = array('d')                 
               eyh_c = array('d')               
               y_up.append(0)  
               x_up.append(0)  
               y_down.append(0)  
               x_down.append(0)  
               for i in range(1,numberOfPoints + 1):
                 x_up.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(i))
                 y_up.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinContent(i) + hlist[sfname+'_'+sf_type+'_SF'].GetBinError(i))     
                 exl_up.append(0)
                 exh_up.append(0)
                 eyl_up.append(0)
                 eyh_up.append(0)
                 x_down.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(i))
                 y_down.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinContent(i) - hlist[sfname+'_'+sf_type+'_SF'].GetBinError(i))     
                 exl_down.append(0)
                 exh_down.append(0)
                 eyl_down.append(0)
                 eyh_down.append(0)
                 x_c.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(i))
                 y_c.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinContent(i))     
                 exl_c.append(0)
                 exh_c.append(0)
                 eyl_c.append(0)
                 eyh_c.append(0)
               x_up[0] = x_up[1]
               y_up[0] = minSF-0.3*sfrange
               x_up.append(ptrange)
               x_up.append(ptrange)
               y_up.append(y_up[numberOfPoints])
               y_up.append(minSF-0.3*sfrange)
               x_down[1] = x_down[1] - 1
               x_down[0] = x_down[1]
               x_down.append(ptrange)
               x_down.append(ptrange)
               y_down[0] = minSF-0.3*sfrange
               y_down.append(y_down[numberOfPoints])
               y_down.append(minSF-0.3*sfrange)
               x_c.append(ptrange)
               y_c.append(y_c[numberOfPoints-1])
               exl_c.append(0)
               exh_c.append(0)
               eyl_c.append(0)
               eyh_c.append(0)
               grs_up = TGraphAsymmErrors(numberOfPoints+3,x_up,y_up,exl_up,exh_up,eyl_up,eyh_up)
               grs_up.SetTitle("TGraphAsymmErrors")
               grs_up.SetMarkerColor(4)
               grs_up.SetMarkerStyle(6)
               grs_up.SetLineWidth(0)
               grs_up.SetLineColor(0)
               grs_up.SetFillColor(4)
               grs_up.SetFillStyle(3004)
               grs_up.GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)
               grs_up.GetXaxis().SetRangeUser(20,ptrange)
               grs_up.GetXaxis().SetTitle("p_{T} [GeV]")
               grs_up.GetXaxis().SetTitleOffset(1.5)
               grs_up.GetYaxis().SetTitle("Data/MC Scale Factors")
               grs_up.GetYaxis().SetTitleOffset(1.8)
               grs_up.Draw("F SAME")
               grs_down = TGraphAsymmErrors(numberOfPoints+3,x_down,y_down,exl_down,exh_down,eyl_down,eyh_down)
               grs_down.SetTitle("TGraphAsymmErrors")
               grs_down.SetMarkerColor(4)
               grs_down.SetMarkerStyle(6)
               grs_down.SetLineWidth(2)
               grs_down.SetLineColor(0)
               grs_down.SetFillColor(0)
               grs_down.SetFillColor(0)
               grs_down.GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)
               grs_down.GetXaxis().SetRangeUser(20,ptrange)
               grs_down.Draw("F SAME")
               grs_c = TGraphAsymmErrors(numberOfPoints+1,x_c,y_c,exl_c,exh_c,eyl_c,eyh_c)
               grs_c.SetTitle("TGraphAsymmErrors")
               grs_c.SetMarkerColor(4)
               grs_c.SetMarkerStyle(6)
               grs_c.SetMarkerSize(1)
               grs_c.SetLineWidth(1)
               grs_c.SetLineColor(4)
               grs_c.GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)
               grs_c.GetXaxis().SetRangeUser(20,ptrange)
               grs_c.Draw("CL SAME")
             if sf_type=='raw': 
               print ("Check!!!!!!!!!")
               print (hlist[sfname+'_'+sf_type+'_SF'].GetNbinsX())
               print (hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(1))
               bins = array('d')
               bins.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(1))
               numberOfBins = 1
               y = array('d')
               eyl = array('d')
               eyh = array('d')
               startingError = hlist[sfname+'_'+sf_type+'_SF'].GetBinError(1)  
               y.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinContent(1))
               eyl.append(startingError)
               eyh.append(startingError)
               #Remove the bins above the data calibration when plotting the extrapolation
               upperLimit = ptrange
               if plotType == "Extra":
                 upperLimit = float(pt_ranges_master_dic["Normal"][jetcol][flav][0]) 
               for i in range(1,hlist[sfname+'_'+sf_type+'_SF'].GetNbinsX()+1):
                 if hlist[sfname+'_'+sf_type+'_SF'].GetBinError(i) != startingError and hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(i) <= upperLimit:
                   startingError = hlist[sfname+'_'+sf_type+'_SF'].GetBinError(i)
                   eyl.append(startingError)
                   eyh.append(startingError)
                   y.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinContent(i))
                   bins.append(hlist[sfname+'_'+sf_type+'_SF'].GetBinLowEdge(i))
                   numberOfBins = numberOfBins + 1
               bins.append(upperLimit)
               x = array('d')
               exl = array('d')
               exh = array('d')
               for i in range(0,len(bins) - 1):
                 x.append((bins[i+1] + bins[i])/2)
                 exl.append((bins[i+1] - bins[i])/2)
                 exh.append((bins[i+1] - bins[i])/2)
               grs = TGraphAsymmErrors(numberOfBins,x,y,exl,exh,eyl,eyh)
               grs.SetTitle("TGraphAsymmErrors")
               grs.SetMarkerColor(1)
               grs.SetMarkerStyle(21)
               grs.SetMarkerSize(1.2)
               grs.SetLineWidth(1)
               grs.GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)
               grs.GetXaxis().SetRangeUser(20,ptrange)
               grs.Draw("P")
 
           if n_ScaleFactors>1:
               if rawCDI_ok:
                if labelDic.has_key(sfname):
                 labelString = ""
                 if labelDic.has_key(sfname):
                   labelString = labelDic[sfname]
                 entry = leglist[0].AddEntry("Smooth",'Measured Scale Factor (total unc.)','LEP')
                 entry.SetLineColor(1);
                 entry.SetLineStyle(1);
                 entry.SetLineWidth(1);
                 entry.SetMarkerColor(1);
                 entry.SetMarkerStyle(21);
                 entry.SetMarkerSize(1);
                 labelString = ""        
                 if labelDic.has_key(sfname):
                   labelString = labelDic[sfname]
                 entry = leglist[0].AddEntry("Smooth",'Smoothed Scale Factor (total unc.)','F')
                 entry.SetLineColor(0);
                 entry.SetFillStyle(3004);
                 entry.SetFillColor(4);
               else:
                 labelString = ""
                 if labelDic.has_key(sfname):
                   labelString = labelDic[sfname]
                 entry = leglist[0].AddEntry("Raw",labelString,'F')
                 entry.SetLineColor(0);
                 entry.SetFillStyle(3004);
                 entry.SetFillColor(4);
               if not drew_tex:
                 draw_tex()
                 drew_tex = True
         draw_tex()
         text = ""
         if sfname ==  "negative_tags_SF":
           text = "negative_tag_Zjet_SF" 
         else:
           text = sfname 
         texlist.append( ROOT.TLatex(0.2,0.84,labelDic[text]) )
         texlist[-1].SetNDC();
         texlist[-1].SetTextFont(42);
         texlist[-1].SetLineWidth(2);
         texlist[-1].SetTextSize(0.03);
         texlist[-1].Draw("same");
         texlist.append( ROOT.TLatex(0.2,0.80, taggerString + " " + "#varepsilon_{b} = " + wp.split("Cut")[1].split("_")[1]  +  " % Single Cut OP"))
         texlist[-1].SetNDC();
         texlist[-1].SetTextFont(42);
         texlist[-1].SetLineWidth(2);
         texlist[-1].SetTextSize(0.03);
         texlist[-1].Draw("same");
         c.cd(1)
         leglist[0].Draw('same')
         gPad.RedrawAxis()
         outfile = ROOT.TFile(outputROOTfilename,'update')
         outf_tagdir = getOrMkdir(outfile,tagger)
         outf_jetCdir  = getOrMkdir(outf_tagdir,jetcol)
         outf_wpdir = getOrMkdir(outf_jetCdir,wp)
         outf_wpdir.WriteTObject(c,'dataMCSF_'+flav+'_'+str(ptrange))
         outfile.Close()
         c.Print(pagename[0:-10]+'/dataMCSF_inEff_'+flav+'_'+str(ptrange)+ '.C')
         c.Print(pagename[0:-10]+'/dataMCSF_inEff_'+flav+'_'+str(ptrange)+'.pdf')
