from array import array
import ROOT
import sys
ROOT.xAOD.Init().ignore()

from ROOT import *
from plot_functions.cosmetics import *

ROOT.gROOT.ProcessLine('.L ./plot_functions/datamc_sf_PC_tagBin.C')
gROOT.SetBatch(1)
gStyle.SetOptStat(0)
ROOT.gStyle.SetPalette(ROOT.kBird)

def getOrMkdir (dir, name):
    dirName = dir.Get(name)
    if dirName:
        return dirName
    return dir.mkdir(name)

def getErrorHists(hist):
  h_SF_up = hist.Clone(hist.GetName()+'_up')
  h_SF_down = hist.Clone(hist.GetName()+'_down')

  for x in range(1,hist.GetXaxis().GetNbins()+1):
    for y in range(1,hist.GetYaxis().GetNbins()+1):
      xval = hist.GetXaxis().GetBinCenter(x)
      yval = hist.GetYaxis().GetBinCenter(y)
      Bin = hist.FindBin(xval,yval)
      h_SF_up.SetBinContent(Bin, hist.GetBinContent(Bin) + hist.GetBinError(Bin) )
      h_SF_down.SetBinContent(Bin, hist.GetBinContent(Bin) - hist.GetBinError(Bin) )
  return [h_SF_up, h_SF_down]

def project_Hist(h,x_or_y):
  if x_or_y=='x':
    nbins = h.GetNbinsY()
    hnew = h.ProjectionX()
  if x_or_y=='y':
    nbins = h.GetNbinsX()
    hnew = h.ProjectionY()
  hnew.Scale(1.0/nbins)
  return hnew

def createSFplot(CDIname, pagename,tagger,jetcol,wp,ptrange_dict,outputROOTfilename, plotType, iteration = "Internal", run = "Run2"):
  print (jetcol,' ',tagger,' ',wp)
   #try to get the number of eta bins in default SF from raw CDI
  cdifile = ROOT.TFile(CDIname,'read')
  tagdir = cdifile.Get(tagger)
  jetcoldir = tagdir.Get(jetcol) 
  wpdir = jetcoldir.Get(wp)
  flav_number = {'B':5,'C':4,'Light':0}
   
  for flav in ['B','C','Light']:
    ptranges = ptrange_dict['dataMCSF'][flav]
    for tagBin in range(1,6):
      for ptrange in ptranges:
        
        nEtaBins = 1
        etabinning = [0,2.5]
        sfobjectlist = []
        flavdir = wpdir.Get(flav)
        for objectlist in flavdir.GetListOfKeys():
          if 'SF' in objectlist.GetName():
            sfobjectlist.append(objectlist.GetName())
        n_ScaleFactors = len(sfobjectlist)
        if n_ScaleFactors < 2:
          return
        sfcolor = {}
        sfcolor['raw'] = [ROOT.kBlack]
        hlist = {}
        maxSF = 0
        minSF = 100000
        c = ROOT.TCanvas('c','c',800,800)
        for i, sfobject in enumerate( sfobjectlist ):
          sfname = sfobject
          print (sfname)
          if not sfname in labelDic:
            if "default_SF" not in sfname:
              continue 
          if "default_SF" not in sfname:
            continue 
          starting = startDic[jetcol][flav] 
          hlist[sfname] =  ROOT.TH2D(sfname,sfname,ptrange-starting,starting,ptrange,nEtaBins,0,2.5)
          
          Fill_SF_Histogram(CDIname, tagger , wp ,jetcol, sfobject[0:-3],hlist[sfname] , flav_number[flav], flav, tagWeights[jetcol][tagger][str(tagBin)])
          h_SF_up, h_SF_down = getErrorHists(hlist[sfname])
          raw_SF = project_Hist(hlist[sfname],'x')
          h_up = project_Hist(h_SF_up,'x')
          h_down = project_Hist(h_SF_down,'x')	
          hlist[sfname+'_raw_SF'] = raw_SF.Clone()
          hlist[sfname+'_raw_SF'].GetXaxis().SetNdivisions(5)
          hlist[sfname+'_raw_SF_up'] = h_up.Clone() 
          hlist[sfname+'_raw_SF_down'] = h_down.Clone()
          if maxSF < h_up.GetMaximum():
            maxSF = h_up.GetMaximum()
          if minSF > h_down.GetMinimum():
            minSF = h_down.GetMinimum()
        texlist = []
        def draw_tex():
          texlist.append( ROOT.TLatex(0.2,0.88,"ATLAS") )
          texlist[-1].SetNDC();
          texlist[-1].SetTextFont(72);
          texlist[-1].SetLineWidth(2);
          texlist[-1].SetTextSize(0.04);
          texlist[-1].Draw('same');
          texlist.append( ROOT.TLatex(0.33,0.88," " + iteration) )
          texlist[-1].SetNDC();
          texlist[-1].SetTextFont(42);
          texlist[-1].SetLineWidth(2);
          texlist[-1].SetTextSize(0.03);
          texlist[-1].Draw("same");  
          if run == "Run3":
            texlist.append( ROOT.TLatex(0.65,0.88,"#sqrt{s} = 13.6 TeV, 29 fb^{-1}") )
          else:
            texlist.append( ROOT.TLatex(0.65,0.88,"#sqrt{s} = 13 TeV, 139 fb^{-1}") )
          texlist[-1].SetNDC();
          texlist[-1].SetTextFont(42);
          texlist[-1].SetLineWidth(1);
          texlist[-1].SetTextSize(0.03);
          texlist[-1].Draw('same');
        
      leglist = []
      leglist.append( ROOT.TLegend(0.19,0.65,0.79,0.75))
      taggerString = tagger
      if tagger == "MV2c10":
        taggerString = "MV2"
      leglist[-1].SetHeader(colDic[jetcol])
      leglist[-1].SetTextFont(42)
      leglist[-1].SetTextSize(0.03)
      leglist[-1].SetLineColor(1)
      leglist[-1].SetLineStyle(1)
      leglist[-1].SetLineWidth(1)
      leglist[-1].SetFillColor(0)
      leglist[-1].SetBorderSize(0)
      leglist[-1].SetFillStyle(0)
      drew_tex = False
      for i, sfobject in enumerate( sfobjectlist ):
        c.cd()  
        sfname = sfobject
        if not sfname in labelDic:
          if "default_SF" not in sfname:
            continue 
        if "default_SF" not in sfname:
          continue 
        sfrange = maxSF - minSF
        h = c.DrawFrame(starting,minSF-0.3*sfrange,ptrange,maxSF+0.8*sfrange)
        h.GetXaxis().SetTitle("p_{T} [GeV]")
        h.GetXaxis().SetTitleOffset(1.5)
        h.GetXaxis().SetNdivisions(5)
        h.GetYaxis().SetTitle(flavDic[flav] + "-jet " + flavTypeDic[flav] + " SFs" )
        h.GetYaxis().SetTitleOffset(1.6)
        c.Modified()
        bins = array('d')
        bins.append(hlist[sfname+'_raw_SF'].GetBinLowEdge(1))
        numberOfBins = 1
        y = array('d')
        eyl = array('d')
        eyh = array('d')
        startingError = hlist[sfname+'_raw_SF'].GetBinError(1)  
        y.append(hlist[sfname+'_raw_SF'].GetBinContent(1))
        eyl.append(startingError)
        eyh.append(startingError)
        #Remove the bins above the data calibration when plotting the extrapolation
        upperLimit = ptrange
        for i in range(1,hlist[sfname+'_raw_SF'].GetNbinsX()+1):
          if hlist[sfname+'_raw_SF'].GetBinError(i) != startingError and hlist[sfname+'_raw_SF'].GetBinLowEdge(i) <= upperLimit:
            startingError = hlist[sfname+'_raw_SF'].GetBinError(i)
            eyl.append(startingError)
            eyh.append(startingError)
            y.append(hlist[sfname+'_raw_SF'].GetBinContent(i))
            bins.append(hlist[sfname+'_raw_SF'].GetBinLowEdge(i))
            numberOfBins = numberOfBins + 1
        bins.append(upperLimit)
        x = array('d')
        exl = array('d')
        exh = array('d')
        for i in range(0,len(bins) - 1):
          x.append((bins[i+1] + bins[i])/2)
          exl.append((bins[i+1] - bins[i])/2)
          exh.append((bins[i+1] - bins[i])/2)
        grs = TGraphAsymmErrors(numberOfBins,x,y,exl,exh,eyl,eyh)
        grs.SetTitle("TGraphAsymmErrors")
        grs.SetMarkerColor(1)
        grs.SetFillStyle(3004)
        grs.SetFillColor(4)
        grs.SetMarkerStyle(21)
        grs.SetMarkerSize(1.2)
        grs.SetLineWidth(1)
        grs.GetYaxis().SetRangeUser(minSF-0.3*sfrange,maxSF+0.8*sfrange)
        grs.GetXaxis().SetRangeUser(20,ptrange)
        gStyle.SetEndErrorSize(5)
        grs.Draw("P")
      
        entry = leglist[0].AddEntry("Smooth",'Measured Scale Factor (total unc.)','LEP')
        entry.SetLineColor(1);
        entry.SetFillColor(4);
        entry.SetFillStyle(3004);
        entry.SetLineStyle(1);
        entry.SetLineWidth(1);
        entry.SetMarkerColor(1);
        entry.SetMarkerStyle(21);
        entry.SetMarkerSize(1);
        if not drew_tex:
          draw_tex()
          drew_tex = True
      draw_tex()
      text = ""
      if sfname ==  "negative_tags_SF":
        text = "negative_tag_Zjet_SF" 
      else:
        text = defaultDic[flav] 
      texlist.append( ROOT.TLatex(0.2,0.84,labelDic[text]) )
      texlist[-1].SetNDC();
      texlist[-1].SetTextFont(42);
      texlist[-1].SetLineWidth(2);
      texlist[-1].SetTextSize(0.03);
      texlist[-1].Draw("same");
      texlist.append( ROOT.TLatex(0.2,0.80, taggerString + ", " + "#varepsilon_{b} #in " + " [" + weightEdge[str(tagBin)][1]  + ","  + weightEdge[str(tagBin)][0] + "] Tag Weight Bin"))
      texlist[-1].SetNDC();
      texlist[-1].SetTextFont(42);
      texlist[-1].SetLineWidth(2);
      texlist[-1].SetTextSize(0.03);
      texlist[-1].Draw("same") 
      c.cd(1)
      leglist[0].Draw('same')
      gPad.RedrawAxis()
      outfile = ROOT.TFile(outputROOTfilename,'update')
      outf_tagdir = getOrMkdir(outfile,tagger)
      outf_jetCdir  = getOrMkdir(outf_tagdir,jetcol)
      outf_wpdir = getOrMkdir(outf_jetCdir,wp)
      outf_wpdir.WriteTObject(c,'dataMCSF_'+flav+'_'+str(ptrange))
      outfile.Close()
      c.Print(pagename[0:-10]+'/dataMCSF_'+flav+'_'+str(ptrange) + "_TagBin"+ str(tagBin) + '.C')
      c.Print(pagename[0:-10]+'/dataMCSF_'+flav+'_'+str(ptrange)+ "_TagBin"+ str(tagBin) + '.pdf')
 



