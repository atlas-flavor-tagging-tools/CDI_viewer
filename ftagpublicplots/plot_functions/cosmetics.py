#!/usr/bin/python3
pt_ranges_master_dic ={
 "Normal" : {
   "AntiKtVR30Rmax4Rmin02TrackJets" :  {'B':[250],'C':[140],'Light':[300]},
   "AntiKt4EMPFlowJets"             :  {'B':[400],'C':[250],'Light':[300]}
 },
 "Extra" : {
   "AntiKtVR30Rmax4Rmin02TrackJets" :  {'B':[3000],'C':[3000],'Light':[3000]},
   "AntiKt4EMPFlowJets"             :  {'B':[3000],'C':[3000],'Light':[3000]}
 }
}

labelDic = {
  "ttbar_PDF_SF"         : "b-jet Calibration with t#bar{t} Events",
  "negative_tag_Zjet_SF" : "light-jet Calibration with Z+jets Events",
  "ttbar1lep_SF"         : "c-jet Calibration with t#bar{t} Events"
}

colDic = {
  "AntiKtVR30Rmax4Rmin02TrackJets": "anti-k_{t} Variable-R Track Jets",
  "AntiKt4EMPFlowJets"            : "anti-k_{t} R=0.4 EMPFlow Jets"
}

startDic = {
  "AntiKtVR30Rmax4Rmin02TrackJets" : {"B":10, "C":10, "Light":10},
  "AntiKt4EMPFlowJets"             : {"B":20,"C":20,"Light":20}
}

binsDic ={
   "AntiKt4EMPFlowJets" :  {
      'B'    :[[20,30],[30,40],[40,60],[60,85],[85,110],[110,140],[140,175],[175,250],[250,400]],
      'C'    :[[20,40],[40,65],[65,140],[140,250]],
      'Light':[[20,50],[50,100],[100,150],[150,300]]
   },
   "AntiKtVR30Rmax4Rmin02TrackJets" :  {
      'B'    :[[10,20],[20,30],[30,60],[60,100],[100,250]],
      'C'    :[[10,20],[20,40],[40,65],[65,140]],
      'Light':[[10,20],[20,30],[30,45],[45,60],[60,150],[150,300]]
   }
}

flavDic = {
  "B"     : "b",
  "C"     : "c",
  "Light" : "l"
}

flavTypeDic = {
  "B"     : "Efficiency",
  "C"     : "Mis-tag Rate",
  "Light" : "Mis-tag Rate"
}

defaultDic = {
  "B"    : "ttbar_PDF_SF",
  "C"    : "ttbar1lep_SF",
  "Light": "negative_tag_Zjet_SF"
}

tagWeights = {
  "AntiKt4EMPFlowJets" : {
    "DL1dv01": {
      "1" :0.5, 
      "2" :1.0, 
      "3" :2.5, 
      "4" :3.5, 
      "5" :5.0
    } 
  },
  "AntiKtVR30Rmax4Rmin02TrackJets" : {
    "DL1dv01": {
      "1" :0.5, 
      "2" :1.5, 
      "3" :2.7, 
      "4" :3.8, 
      "5" :5.0
    }
  }
}

weightEdge = {
    "1" :["100%","85%"], 
    "2" :["85%","77%"], 
    "3" :["77%","70%"], 
    "4" :["70%","60%"], 
    "5" :["60%","0%"]
}

binLabels = ["[100%,85%]","[85%,77%]","[77%,70%]","[70%,60%]","[60%,0%]"]
binLabelLocations = [0.16,0.33,0.486,0.646,0.81]
